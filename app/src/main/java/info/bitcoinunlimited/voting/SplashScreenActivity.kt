package info.bitcoinunlimited.voting

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.utils.TAG_VOTE
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        initPrivateKey()
    }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_VOTE, "${SplashScreenActivity::class}: ${exception.message}")
        renderErrorState(exception)
        throw exception
    }

    private fun initPrivateKey() = GlobalScope.launch(Dispatchers.IO + handler) {
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContext).mnemonicDao()
        val walletRepository = WalletDatabase.getInstance(mnemonicDao)
        walletRepository.initWallet(applicationContext)
        val currentUser = walletRepository.getOrAwaitPaydestination()
        val privateKey = currentUser.secret?.getSecret() ?: throw Exception("Cannot get private-key in startVotePeerActivity()")
        startVotingActivity(privateKey)
    }

    private fun startVotingActivity(privateKey: ByteArray) = GlobalScope.launch(Dispatchers.Main + handler) {
        val intent = InjectorUtilsApp.getVotingActivityIntent(applicationContext, privateKey, intent)
        startActivity(intent)
    }

    private fun renderErrorState(exception: Throwable) {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.something_went_wrong))
            .setMessage(exception.message)
            .setNeutralButton(resources.getString(R.string.got_it)) { _, _ ->
                // Do nothing.
            }
            .show()
    }
}
