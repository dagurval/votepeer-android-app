package info.bitcoinunlimited.voting.messaging

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Log.d(TAG_NOTIFICATIONS, "Refreshed FCM token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        handleNewToken(token)
    }

    private fun handleNewToken(token: String) = GlobalScope.launch(Dispatchers.IO) {
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContext).mnemonicDao()
        val walletRepository = WalletDatabase.getInstance(mnemonicDao)
        MyMessagingService(applicationContext, walletRepository).handleNewToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        handleMessage(remoteMessage)
    }

    private fun handleMessage(remoteMessage: RemoteMessage) = GlobalScope.launch(Dispatchers.IO) {
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContext).mnemonicDao()
        val walletDatabase = WalletDatabase.getInstance(mnemonicDao)
        val payDestination = walletDatabase.getOrAwaitPaydestination()
        MyMessagingService(applicationContext, walletDatabase).handleRemoteMessage(remoteMessage, payDestination)
    }

    companion object {
        const val TAG_NOTIFICATIONS = "MyFirebaseMsgService"
    }
}
