package info.bitcoinunlimited.voting.messaging

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import bitcoinunlimited.libbitcoincash.PayDestination
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.voting.CloudLogger
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.utils.TAG_VOTE
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import org.json.JSONObject

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MyMessagingService(
    private val applicationContext: Context,
    private val walletDatabase: WalletDatabase
) {
    internal fun handleRemoteMessage(
        remoteMessage: RemoteMessage,
        payDestination: PayDestination
    ) = GlobalScope.launch(Dispatchers.Main) {
        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            when (getPushType(remoteMessage)) {
                "on_createElection" -> {
                    val electionId = getElectionId(remoteMessage)
                    val privateKey = payDestination.secret?.getSecret() ?: throw Exception("Cannot get privateKey")
                    val intent = InjectorUtilsApp.getVotePeerActivityIntent(applicationContext, privateKey, electionId)
                    val pendingIntent = getVotePeerActivityPendingIntent(intent)
                    val notificationBuilder = createNotificationBuilder(electionId, pendingIntent)
                    createNotification(notificationBuilder)
                }
                else -> {
                    println("do nothing")
                }
            }
        }
    }

    internal fun getElectionId(remoteMessage: RemoteMessage): String {
        val electionId = "election_id"
        val data = remoteMessage.data as Map<*, *>? ?: throw Exception("Data is missing in remoteMessage: $remoteMessage")
        val remoteMessageData = JSONObject(data)
        if (remoteMessageData.has(electionId)) {
            return remoteMessageData[electionId] as String
        } else {
            throw Exception("election_id is missing from remoteMessage data: $remoteMessage")
        }
    }

    internal fun getPushType(remoteMessage: RemoteMessage): String {
        val pushType = "pushType"
        val params = remoteMessage.data as Map<*, *>? ?: throw Exception("Data is missing in remoteMessage: $remoteMessage")
        val remoteMessageData = JSONObject(params)
        if (remoteMessageData.has(pushType)) {
            return remoteMessageData[pushType] as String
        } else {
            throw Exception("pushType is missing from remoteMessage.data: $remoteMessage")
        }
    }

    internal fun handleNewToken(token: String) = GlobalScope.launch(Dispatchers.IO + handler) {
        val payDestination = walletDatabase.getOrAwaitPaydestination()
        val authRepository = AuthRepository.getInstance(payDestination)
        authRepository.setAndroidNotificationToken(token)
    }

    internal fun getVotePeerActivityPendingIntent(intent: Intent): PendingIntent {
        return PendingIntent.getActivity(
            applicationContext,
            0 /* Request code */,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )
    }

    internal fun createNotificationBuilder(
        content: String,
        pendingIntent: PendingIntent
    ): NotificationCompat.Builder {
        val channelId = "default_notification_channel_id"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        return NotificationCompat.Builder(applicationContext, channelId)
            .setContentText(content)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_baseline_how_to_vote_24)
    }

    internal fun createNotification(
        notificationBuilder: NotificationCompat.Builder
    ) = GlobalScope.launch(Dispatchers.Main + handler) {
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "default_notification_channel_id"

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "VotePeer",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_VOTE, exception.message ?: "error in MyMessagingService")
        CloudLogger.recordException(exception)
        Toast.makeText(applicationContext, exception.message ?: "error in MyMessagingService", Toast.LENGTH_LONG).show()
    }
}
