package info.bitcoinunlimited.voting.utils

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.UnsecuredSecret
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.voting.VotingActivity
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.identity.IdentityViewModelFactory
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewModel
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewModel
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
object InjectorUtilsApp {
    fun provideIdentityViewModelFactory(fragment: Fragment, privateKey: ByteArray): IdentityViewModelFactory {
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, UnsecuredSecret(privateKey))
        val address = payDestination.address ?: throw Exception("Cannot get payDestination.address")
        val electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)

        return IdentityViewModelFactory(fragment, electrumAPI, address)
    }

    fun provideMnemonicViewModelFactory(
        fragment: Fragment,
        privateKeyHex: String,
    ): MnemonicViewModel.MMnemonicViewModelFactory {
        val context = fragment.requireContext()
        val walletRepository = getWalletRepository(context)
        return MnemonicViewModel.MMnemonicViewModelFactory(walletRepository, privateKeyHex, fragment)
    }

    fun provideRecoverViewModelFactory(
        app: Application,
        fragment: Fragment,
        privateKey: ByteArray
    ): RecoverViewModel.RecoverViewModelFactory {
        val context = fragment.requireContext()
        val walletRepository = getWalletRepository(context)
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, UnsecuredSecret(privateKey))
        val authRepository = AuthRepository.getInstance(payDestination)
        return RecoverViewModel.RecoverViewModelFactory(walletRepository, authRepository, app)
    }

    private fun getWalletRepository(context: Context): WalletDatabase {
        val electionDao = MnemonicDatabase.getInstance(context.applicationContext).mnemonicDao()
        return WalletDatabase.getInstance(electionDao)
    }

    fun getVotingActivityIntent(applicationContext: Context, privateKey: ByteArray, intent: Intent): Intent {
        val electionId = intent.getStringExtra("election_id")
        return getVotePeerActivityIntent(applicationContext, privateKey, electionId)
    }

    internal fun getVotePeerActivityIntent(applicationContext: Context, privateKey: ByteArray, electionId: String?): Intent {
        return Intent(applicationContext, VotingActivity::class.java)
            .putExtra(Constants.PRIVATE_KEY, privateKey)
            .putExtra("election_id", electionId)
            .putExtra("setSupportActionBar", false)
            .putExtra(Constants.SETUP_ACTION_BAR_WITH_NAV_CONTROLLER, false)
            .putExtra("setContentView", false)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }
}
