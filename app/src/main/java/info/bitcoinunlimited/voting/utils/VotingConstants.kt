package info.bitcoinunlimited.voting.utils

object VotingConstants {
    const val privateKeyHex = "privateKeyHex"
    const val mnemonicTableId = "mnemonic"
}
