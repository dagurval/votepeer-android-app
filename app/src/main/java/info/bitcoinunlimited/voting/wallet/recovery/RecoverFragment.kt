package info.bitcoinunlimited.voting.wallet.recovery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.voting.databinding.FragmentRecoverBinding
import info.bitcoinunlimited.voting.utils.Event
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class RecoverFragment : Fragment(), RecoverView {
    private lateinit var binding: FragmentRecoverBinding
    private var recoveringFromMnemonicAlertDialog: AlertDialog? = null
    private val safeArgs: RecoverFragmentArgs by navArgs()

    private val viewModel: RecoverViewModel by viewModels {
        InjectorUtilsApp.provideRecoverViewModelFactory(
            activity?.application ?: throw Exception("Cannot get application in RecoverFragmeent"),
            this,
            UtilStringEncoding.hexToByteArray(safeArgs.privateKeyHex)
        )
    }
    private val intent = RecoverViewIntent()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecoverBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.bindIntents(this)
        initClickListeners()
    }

    private fun initClickListeners() {
        binding.replaceMnemonicButton.setOnClickListener {
            val mnemonicPhrase = binding.oldMnemonicInput.text.toString()
            intent.recoverMnemonic.value = Event(
                RecoverViewIntent.RecoverMnemonic(
                    Mnemonic(mnemonicPhrase),
                    activity?.applicationContext
                        ?: throw Exception("Cannot find applicationContext in RecoverFragment")
                )
            )
        }
    }

    override fun initState() = intent.initState

    override fun recoverFromMnemonic() = intent.recoverMnemonic

    override fun render(state: RecoverViewState) {
        when (state) {
            is Recovery -> println("Default state")
            is Recovering -> renderRecovering(state)
            is RecoverySuccess -> renderRecoverySuccess(state)
            is RecoveryError -> renderRecoveryError(state)
        }

        if (state is Recovery || state is RecoverySuccess || state is RecoveryError)
            renderLoading(false)
        else if (state is Recovering)
            renderLoading(true)

        if (state is RecoverySuccess || state is RecoveryError)
            recoveringFromMnemonicAlertDialog?.hide()
    }

    private fun renderRecovering(state: Recovering) {
        recoveringFromMnemonicAlertDialog = MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Recovering Mnemonic...")
            .setMessage(state.message)
            .setNeutralButton("ok") { _, _ ->
                // Do nothing.
            }
            .show()
    }

    private fun renderRecoverySuccess(recoverySuccess: RecoverySuccess) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Success")
            .setMessage(recoverySuccess.message)
            .setNeutralButton("ok") { _, _ ->
                // Do nothing.
            }
            .show()
            .setOnDismissListener {
                findNavController().popBackStack()
            }
    }

    private fun renderRecoveryError(recoveryError: RecoveryError) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Error")
            .setMessage(recoveryError.message)
            .setNeutralButton("Got it") { _, _ ->
                // Do nothing.
            }
            .show()
    }

    private fun renderLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.replaceMnemonicButton.visibility = View.GONE
            binding.recoveringMnemonicProgressBar.visibility = View.VISIBLE
        } else {
            binding.replaceMnemonicButton.visibility = View.VISIBLE
            binding.recoveringMnemonicProgressBar.visibility = View.GONE
        }
    }
}
