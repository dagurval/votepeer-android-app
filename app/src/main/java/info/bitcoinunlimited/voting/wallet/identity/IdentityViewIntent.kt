package info.bitcoinunlimited.voting.wallet.identity

import info.bitcoinunlimited.voting.utils.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class IdentityViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val refreshBalance: MutableStateFlow<Event<RefreshBalance?>> = MutableStateFlow(Event(null))
) {
    object RefreshBalance
}
