package info.bitcoinunlimited.voting.wallet.recovery

import android.app.Application
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.voting.CloudLogger
import info.bitcoinunlimited.voting.utils.onEachEvent
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class RecoverViewModel(
    private val walletDatabase: WalletDatabase,
    private var authRepository: AuthRepository,
    application: Application
) : AndroidViewModel(application) {
    internal val state = MutableStateFlow<RecoverViewState?>(null)
    private val errorHandler = CoroutineExceptionHandler { context, exception ->
        CloudLogger.recordException(exception)
        val message = exception.localizedMessage ?: exception.message ?: "Something went wrong: $exception, $context"
        state.value = RecoveryError(message)
    }

    fun bindIntents(view: RecoverView) {
        view.initState().onEach {
            state.value = Recovery
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope + errorHandler)

        view.recoverFromMnemonic().onEachEvent { intent ->
            viewModelScope.launch(Dispatchers.IO + errorHandler) {
                val mnemonicPhrase = intent.mnemonic.phrase
                val state = Recovering("Recovering with Mnemonic: $mnemonicPhrase")
                setState(state)
                recoverFromMnemonic(intent.mnemonic)
                setState(RecoverySuccess("The recovery was successful for Mnemonic: $mnemonicPhrase"))
            }
        }.launchIn(viewModelScope + errorHandler)
    }

    // TODO: Move to authRepository
    internal suspend fun recoverFromMnemonic(mnemonic: Mnemonic) {
        val mnemonicPhrase = mnemonic.phrase

        if (mnemonicPhrase.isEmpty()) {
            setState(RecoveryError("Enter backup phrase before submitting!"))
        } else {
            val payDestination = walletDatabase.recoverFromMnemonic(getApplication<Application>().applicationContext, mnemonic)
            // TODO: Check that all instances of authRepository uses the new private key/payDestination
            authRepository = AuthRepository.getInstance(payDestination)
            authRepository.signInWithKeyPair()
            authRepository.updateNotificationToken()
        }
    }

    internal fun setState(newState: RecoverViewState) = viewModelScope.launch(Dispatchers.Main + errorHandler) {
        state.value = newState
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    class RecoverViewModelFactory(
        private val walletDatabase: WalletDatabase,
        private val authRepository: AuthRepository,
        private val application: Application
    ) : ViewModelProvider.AndroidViewModelFactory(application) {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RecoverViewModel(walletDatabase, authRepository, application) as T
        }
    }
}
