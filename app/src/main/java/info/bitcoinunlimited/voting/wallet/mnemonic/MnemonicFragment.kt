package info.bitcoinunlimited.voting.wallet.mnemonic

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import info.bitcoinunlimited.voting.CloudLogger
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.databinding.MnemonicFragmentBinding
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.utils.VotingConstants
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewState.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MnemonicFragment : Fragment(), MnemonicView {
    private lateinit var binding: MnemonicFragmentBinding
    private val safeArgs: MnemonicFragmentArgs by navArgs()

    val viewModel: MnemonicViewModel by viewModels {
        InjectorUtilsApp.provideMnemonicViewModelFactory(
            this,
            safeArgs.privateKeyHex
        )
    }
    private val intent = MnemonicViewIntent()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MnemonicFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.bindIntents(this)
        initClickListeners()
    }

    override fun onResume() {
        super.onResume()
        val handler = CoroutineExceptionHandler { context, throwable ->
            val message = throwable.message ?: "Something went wrong in ElectionMasterFragment"
            Log.e(TAG_MNEMONIC_FRAGMENT, context.toString())
            Log.e(TAG_MNEMONIC_FRAGMENT, message)
            CloudLogger.recordException(throwable)
            viewModel.state.value = MnemonicErrorMessage(message)
        }
        viewModel.viewModelScope.launch(Dispatchers.IO + handler) {
            viewModel.fetchMnemonic()
        }
        hideKeyBoard()
    }

    private fun initClickListeners() {
        binding.recoverMnemonicButton.setOnClickListener { _ ->
            val args = bundleOf(VotingConstants.privateKeyHex to safeArgs.privateKeyHex)
            findNavController().navigate(R.id.nav_recover, args)
        }
    }

    override fun initState() = intent.initState
    override fun submitMnemonic() = intent.submitMnemonic

    override fun render(state: MnemonicViewState) {
        when (state) {
            is MnemonicAvailable -> renderSubmitMnemonicShow(state)
            is MnemonicErrorMessage -> renderSubmitMnemonicSubmitError(state)
        }
    }

    private fun renderSubmitMnemonicShow(mnemonicAvailable: MnemonicAvailable) {
        binding.mnemonicDisplay.text = mnemonicAvailable.mnemonic.phrase
    }

    private fun renderSubmitMnemonicSubmitError(mnemonicSubmitError: MnemonicErrorMessage) {
        binding.mnemonicErrorMessage.text = mnemonicSubmitError.message
    }

    private fun hideKeyBoard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager ?: return
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    companion object {
        const val TAG_MNEMONIC_FRAGMENT = "ElectionDetailFragment"
    }
}
