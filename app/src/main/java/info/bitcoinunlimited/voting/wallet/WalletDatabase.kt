package info.bitcoinunlimited.voting.wallet

import android.content.Context
import android.util.Log
import bitcoinunlimited.libbitcoincash.Bip44Wallet
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.GenerateBip39SecretWords
import bitcoinunlimited.libbitcoincash.GenerateEntropy
import bitcoinunlimited.libbitcoincash.OpenKvpDB
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.PlatformContext
import info.bitcoinunlimited.voting.utils.TAG_WALLET_REPOSITORY
import info.bitcoinunlimited.voting.utils.VotingConstants
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import info.bitcoinunlimited.voting.wallet.room.MnemonicDao
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
class WalletDatabase(private val mnemonicDao: MnemonicDao) {
    private val _wallet: MutableStateFlow<Bip44Wallet?> = MutableStateFlow(null)
    val wallet: StateFlow<Bip44Wallet?> = _wallet.asStateFlow()
    private val dbPrefix = "voter.cash"

    suspend fun initWallet(context: Context, dbPrefix: String = "voter.cash"): Boolean {
        if (wallet.value != null) {
            return false
        }
        val mnemonic = getMnemonic()
        val platFormContext = PlatformContext(context)
        val walletDb = OpenKvpDB(platFormContext, dbPrefix + "bip44walletdb")
            ?: throw Exception("Cannot init wallet! OpenKvpDB returns null in initWallet()")
        _wallet.value = Bip44Wallet(
            walletDb,
            "my-wallet",
            ChainSelector.BCHMAINNET,
            mnemonic.phrase
        )
        return true
    }

    suspend fun recoverFromMnemonic(context: Context, mnemonic: Mnemonic): PayDestination {
        val platformContext = PlatformContext(context)
        val walletDb = OpenKvpDB(platformContext, dbPrefix + "bip44walletdb")

        val wallet = Bip44Wallet(
            walletDb!!,
            "my-wallet",
            ChainSelector.BCHMAINNET,
            mnemonic.phrase
        )
        _wallet.value = wallet
        mnemonicDao.insert(mnemonic)
        return wallet.getDestinationAtIndex(0)
    }

    suspend fun getMnemonic(): Mnemonic {
        val mnemonicLocalStorage = mnemonicDao.getMnemonic(VotingConstants.mnemonicTableId)
        return if (mnemonicLocalStorage == null) {
            Log.i(TAG_WALLET_REPOSITORY, "Mnemonic not found. Generating new mnemonic.")
            val entropy = GenerateEntropy(128)
            val words = GenerateBip39SecretWords(entropy)
            val mnemonic = Mnemonic(words)

            mnemonicDao.insert(mnemonic)
            mnemonic
        } else {
            mnemonicLocalStorage
        }
    }

    suspend fun getOrAwaitPaydestination(): PayDestination {
        if (wallet.value !== null) {
            return wallet.value?.getDestinationAtIndex(0) ?: throw Exception("Cannot get wallet from wallet.value")
        } else {
            lateinit var payDestination: PayDestination
            wallet.collect { newPayDestination ->
                if (newPayDestination != null) {
                    payDestination = newPayDestination.getDestinationAtIndex(0)
                    return@collect
                }
            }
            return payDestination
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: WalletDatabase? = null

        fun getInstance(mnemonicDao: MnemonicDao) =
            instance ?: synchronized(this) {
                instance ?: WalletDatabase(mnemonicDao).also { instance = it }
            }
    }
}
