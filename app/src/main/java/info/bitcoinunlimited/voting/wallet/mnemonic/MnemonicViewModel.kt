package info.bitcoinunlimited.voting.wallet.mnemonic

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.savedstate.SavedStateRegistryOwner
import info.bitcoinunlimited.voting.utils.onEachEvent
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewIntent.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewState.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
class MnemonicViewModel(
    private val walletDatabase: WalletDatabase,
    internal val privateKeyHex: String
) : ViewModel() {
    internal val state = MutableStateFlow<MnemonicViewState?>(null)

    init {
        viewModelScope.launch(Dispatchers.IO) {
            fetchMnemonic()
        }
    }

    internal suspend fun fetchMnemonic() {
        val mnemonic = walletDatabase.getMnemonic().phrase
        state.value = MnemonicAvailable(Mnemonic(mnemonic))
    }

    internal fun bindIntents(view: MnemonicView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope)

        view.submitMnemonic().onEachEvent { submitMnemonic ->
            submitMnemonic(submitMnemonic)
        }.launchIn(viewModelScope)
    }

    internal fun submitMnemonic(submitMnemonic: SubmitMnemonic) {
        val mnemonic = Mnemonic(submitMnemonic.phrase)

        // TODO: add more form control and validate mnemonic format
        // NOTE: Use this? https://github.com/NovaCrypto/BIP39
        if (mnemonic.phrase.isEmpty())
            state.value = MnemonicErrorMessage("Mnemonic is empty!")
        else
            state.value = MnemonicAvailable(mnemonic)
    }

    @ExperimentalCoroutinesApi
    @InternalCoroutinesApi
    class MMnemonicViewModelFactory(
        private val walletDatabase: WalletDatabase,
        private val privateKeyHex: String,
        owner: SavedStateRegistryOwner
    ) : AbstractSavedStateViewModelFactory(owner, null) {
        override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
            MnemonicViewModel(walletDatabase, privateKeyHex) as T
    }
}
