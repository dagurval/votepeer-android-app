package info.bitcoinunlimited.voting.wallet.recovery

sealed class RecoverViewState {
    object Recovery : RecoverViewState()

    data class RecoverySuccess(
        val message: String
    ) : RecoverViewState()

    data class RecoveryError(
        val message: String
    ) : RecoverViewState()

    data class Recovering(
        val message: String
    ) : RecoverViewState()
}
