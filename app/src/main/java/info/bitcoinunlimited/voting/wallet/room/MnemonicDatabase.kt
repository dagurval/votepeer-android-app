package info.bitcoinunlimited.voting.wallet.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Mnemonic::class], version = 4, exportSchema = false)
abstract class MnemonicDatabase : RoomDatabase() {
    abstract fun mnemonicDao(): MnemonicDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: MnemonicDatabase? = null

        fun getInstance(context: Context): MnemonicDatabase {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
                }
        }

        private fun buildDatabase(context: Context): MnemonicDatabase {
            return Room.databaseBuilder(
                context, MnemonicDatabase::class.java,
                "mnemonic-db"
            ).fallbackToDestructiveMigration().build()
        }
    }
}
