package info.bitcoinunlimited.voting.wallet.mnemonic

import info.bitcoinunlimited.voting.wallet.room.Mnemonic

sealed class MnemonicViewState {
    data class MnemonicAvailable(
        val mnemonic: Mnemonic
    ) : MnemonicViewState()

    data class MnemonicErrorMessage(
        val message: String
    ) : MnemonicViewState()
}
