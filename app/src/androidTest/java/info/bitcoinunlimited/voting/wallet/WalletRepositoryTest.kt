package info.bitcoinunlimited.voting.wallet

import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.platform.app.InstrumentationRegistry
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voting.VotePeerApplication
import info.bitcoinunlimited.voting.wallet.room.MnemonicDao
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import io.mockk.coVerify
import io.mockk.spyk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.jupiter.api.Assertions.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
internal class WalletRepositoryTest {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    private lateinit var context: Context
    private lateinit var electionDao: MnemonicDao
    private lateinit var walletDatabase: WalletDatabase
    private val dispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(dispatcher)
    private lateinit var app: VotePeerApplication

    @BeforeEach
    fun before() {
        app = getApplicationContext()
        context = InstrumentationRegistry.getInstrumentation().context
        electionDao = MnemonicDatabase.getInstance(app.applicationContext).mnemonicDao()
        walletDatabase = spyk(WalletDatabase.getInstance(electionDao))
    }

    @Test
    fun getsEmptyWallet() {
        assertNull(walletDatabase.wallet.value)
    }

    @Test
    fun initsAndGetsWallet() = runBlocking {
        val applicationContext = app.applicationContext
        walletDatabase.initWallet(applicationContext)
        coVerify { walletDatabase.getMnemonic() }
        assertNotNull(walletDatabase.wallet.value)
    }

    @Test
    fun returnsFalseIfWalletIsInitialized() = runBlocking {
        val applicationContext = app.applicationContext
        walletDatabase.initWallet(applicationContext)

        val reInitialized = walletDatabase.initWallet(applicationContext)
        assertFalse(reInitialized)
    }

    @Test
    fun recoversFromMnemonic() {
        val applicationContext = app.applicationContext
        val mnemonic = WalletMock.getMnemonic()
        testScope.launch {
            walletDatabase.recoverFromMnemonic(applicationContext, mnemonic)
        }
        assertNotNull(walletDatabase.wallet.value)
    }

    @Test
    fun getsMnemonic() = runBlocking {
        val mnemonic = walletDatabase.getMnemonic()
        assertNotNull(mnemonic)
        assertTrue(mnemonic.phrase.isNotEmpty())
    }

    @Test
    fun getsPayDestination() = runBlocking {
        val applicationContext = app.applicationContext
        var payDestination: PayDestination? = null
        walletDatabase.initWallet(applicationContext)
        payDestination = walletDatabase.getOrAwaitPaydestination()
        assertNotNull(payDestination)
    }
}
