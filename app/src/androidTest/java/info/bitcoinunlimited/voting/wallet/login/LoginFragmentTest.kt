package info.bitcoinunlimited.voting.wallet.login

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import bitcoinunlimited.libbitcoincash.ToHexStr
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.VotePeerMock.mockPrivateKey
import info.bitcoinunlimited.voting.utils.VotingConstants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class LoginFragmentTest {
    private lateinit var scenario: FragmentScenario<LoginFragment>

    @Before fun init() {
        val privateKeyMock = mockPrivateKey()
        val privateKeyHex = ToHexStr(privateKeyMock)
        val fragmentArgs = bundleOf(VotingConstants.privateKeyHex to privateKeyHex)
        scenario = launchFragmentInContainer(fragmentArgs)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test fun testClickingQrScanner() {
        onView(withId(R.id.website_login_qr_scanner)).perform(click())
    }

    @Test fun verifyTextHeadline() {
        val textHeadline = "Login: Scan QR-code at https://voter.cash"
        onView(withId(R.id.headline_login)).check(matches(withText(textHeadline)))
    }

    @Test fun verifyTextStep1() {
        val textStep1 = "1. open https://voter.cash on your computer"
        onView(withId(R.id.step1_login)).check(matches(withText(textStep1)))
    }

    @Test fun verifyTextStep2() {
        val textStep2 = "2. Press Login to hold an election-button"
        onView(withId(R.id.step2_login)).check(matches(withText(textStep2)))
    }

    @Test fun verifyTextStep3() {
        val textStep3 = "3. Use button below to scan the QR-code"
        onView(withId(R.id.step3_login)).check(matches(withText(textStep3)))
    }
}
