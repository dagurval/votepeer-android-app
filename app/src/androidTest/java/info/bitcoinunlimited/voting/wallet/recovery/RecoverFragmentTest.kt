package info.bitcoinunlimited.voting.wallet.recovery

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.* // ktlint-disable no-wildcard-imports
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.ToHexStr
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.utils.VotingConstants
import info.bitcoinunlimited.voting.wallet.WalletMock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class RecoverFragmentTest {
    private lateinit var scenario: FragmentScenario<RecoverFragment>
    private lateinit var navController: TestNavHostController
    private val privateKeyMock = VotePeerMock.mockPrivateKey()

    @BeforeEach
    fun setUp() {
        val privateKeyHex = ToHexStr(privateKeyMock)
        val fragmentArgs = bundleOf(VotingConstants.privateKeyHex to privateKeyHex)

        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        scenario = launchFragmentInContainer(fragmentArgs, R.style.AppTheme)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun recoverMnemonicTest() {
        val mnemonicPhraseMock = WalletMock.getMnemonic().phrase

        onView(withId(R.id.old_mnemonic_input))
            .perform(typeText(mnemonicPhraseMock))
        onView(isRoot()).perform(closeSoftKeyboard())
        onView(withId(R.id.replace_mnemonic_button))
            .perform(click())

        onView(withText("Recovering Mnemonic..."))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        onView(withText("ok"))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())

        onView(withText("The recovery was successful for Mnemonic: $mnemonicPhraseMock"))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }

    @Test
    fun recoverEmptyMnemonicTest() {
        val mnemonicPhraseMock = ""

        onView(withId(R.id.old_mnemonic_input))
            .perform(typeText(mnemonicPhraseMock))
        onView(isRoot()).perform(closeSoftKeyboard())
        onView(withId(R.id.replace_mnemonic_button))
            .perform(click())

        onView(withText("Enter backup phrase before submitting!"))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
    }
}
