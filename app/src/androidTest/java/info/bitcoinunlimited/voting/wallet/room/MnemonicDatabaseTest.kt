package info.bitcoinunlimited.voting.wallet.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import info.bitcoinunlimited.voting.utils.VotingConstants.mnemonicTableId
import info.bitcoinunlimited.voting.wallet.WalletMock
import java.io.IOException
import kotlin.jvm.Throws
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class MnemonicDatabaseTest {
    private lateinit var mnemonicDao: MnemonicDao
    private lateinit var db: MnemonicDatabase

    @Before fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, MnemonicDatabase::class.java).build()
        mnemonicDao = db.mnemonicDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetMnemonicTest() = runBlocking {
        val mnemonicMock = WalletMock.getMnemonic()
        mnemonicDao.insert(mnemonicMock)
        val currentMnemonic = mnemonicDao.getMnemonic(mnemonicTableId)
        assertThat(currentMnemonic, equalTo(mnemonicMock))
    }
}
