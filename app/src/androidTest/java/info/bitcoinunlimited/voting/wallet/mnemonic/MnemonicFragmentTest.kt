package info.bitcoinunlimited.voting.wallet.mnemonic

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.ToHexStr
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.wallet.WalletMock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MnemonicFragmentTest {
    private lateinit var scenario: FragmentScenario<MnemonicFragment>
    private lateinit var navController: TestNavHostController
    private val privateKey = VotePeerMock.mockPrivateKey()
    private val privateKeyHex = ToHexStr(privateKey)
    private val fragmentArgs = bundleOf(
        "privateKeyHex" to privateKeyHex
    )
    @BeforeEach fun init() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        scenario = launchFragmentInContainer(fragmentArgs, R.style.AppTheme)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun displayUiElements() {
        onView(withId(R.id.mnemonic_text_view)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_display)).check(matches(isDisplayed()))
        onView(withId(R.id.recover_mnemonic_button)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_error_message)).check(matches(isDisplayed()))
    }

    @Test fun renderMnemonicTest() = runBlockingTest {
        val mnemonicMock = WalletMock.getMnemonic()
        scenario.onFragment { fragment ->
            fragment.render(MnemonicViewState.MnemonicAvailable(mnemonicMock))
        }
        onView(withId(R.id.mnemonic_display)).check(matches(withText(mnemonicMock.phrase)))
    }

    @Test fun renderMnemonicError() {
        val errorMessageMock = "Something went wrong!"
        scenario.onFragment { fragment ->
            fragment.render(MnemonicViewState.MnemonicErrorMessage(errorMessageMock))
        }
        onView(withId(R.id.mnemonic_error_message)).check(matches(withText(errorMessageMock)))
    }

    @Test fun testNavigateToRecover() {
        scenario.onFragment { fragment ->
            navController.setGraph(R.navigation.nav_graph_main)
            navController.setCurrentDestination(R.id.nav_mnemonic)
            Navigation.setViewNavController(fragment.requireView(), navController)
        }

        onView(withId(R.id.recover_mnemonic_button)).perform(click())
        val currentDestinationId = navController.currentDestination?.id
        assertEquals(currentDestinationId, R.id.nav_recover)
        assertNotEquals(currentDestinationId, R.id.nav_mnemonic)
    }
}
