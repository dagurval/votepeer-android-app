package info.bitcoinunlimited.voting.wallet.recovery

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import info.bitcoinunlimited.votepeer.auth.AuthRepository
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import io.mockk.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
internal class RecoverViewModelTest {
    private lateinit var viewModel: RecoverViewModel
    private lateinit var walletDatabase: WalletDatabase
    private lateinit var authRepository: AuthRepository
    private lateinit var application: Application
    private val recoveryViewStateMock = RecoverViewState.Recovery

    @BeforeEach
    fun setUp() = runBlocking {
        application = mockk(relaxed = true)
        val applicationContext = ApplicationProvider.getApplicationContext() as Context
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContext).mnemonicDao()
        walletDatabase = spyk(WalletDatabase.getInstance(mnemonicDao))
        walletDatabase.initWallet(applicationContext)
        val identity = walletDatabase.getOrAwaitPaydestination()
        authRepository = spyk(AuthRepository.getInstance(identity))
        viewModel = spyk(RecoverViewModel(walletDatabase, authRepository, application))
    }

    @AfterEach
    fun tearDown() {
        viewModel.state.value = null
    }

    @Test
    fun bindIntents() {
        val recoverFragment = mockk<RecoverFragment>(relaxed = true)
        viewModel.state.value = recoveryViewStateMock
        viewModel.bindIntents(recoverFragment)
        coVerify { viewModel.state.filterNotNull() }
        assertEquals(recoveryViewStateMock, viewModel.state.value)
    }
}
