package info.bitcoinunlimited.voting.messaging

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.wallet.WalletDatabase
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MyMessagingServiceTest {

    private lateinit var myMessagingService: MyMessagingService
    private val electionIdMock = "election_id_mock_value"
    private lateinit var privateKeyMock: ByteArray
    private lateinit var remoteMessageMock: RemoteMessage
    private lateinit var applicationContextMock: Context

    @BeforeEach
    fun setUp() {
        applicationContextMock = ApplicationProvider.getApplicationContext() as Context
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContextMock).mnemonicDao()
        val walletRepository = WalletDatabase.getInstance(mnemonicDao)
        myMessagingService = MyMessagingService(applicationContextMock, walletRepository)
        privateKeyMock = VotePeerMock.mockPrivateKey()

        val pushTypeKey = "pushType"
        val pushTypeMock = "on_create_election"
        val electionIdKey = "election_id"
        val electionIdMock = "election_id_mock_value"
        val remoteMessageData = mapOf(Pair(electionIdKey, electionIdMock), Pair(pushTypeKey, pushTypeMock))
        remoteMessageMock = mockk(relaxed = true)
        every { remoteMessageMock.data } returns remoteMessageData
    }

    @Test
    fun `handleRemoteMessage$app_debug`() {
        // TODO: Implement
    }

    @Test
    fun getElectionIdNoData() {
        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
        val exception = assertThrows<Exception> {
            myMessagingService.getElectionId(remoteMessageMock)
        }
        assertEquals(exception.message, "election_id is missing from remoteMessage data: $remoteMessageMock")
    }

    @Test
    fun getElectionId() {
        val electionId = myMessagingService.getElectionId(remoteMessageMock)
        assertEquals(electionIdMock, electionId)
    }

    @Test
    fun getPushType() {
        val pushTypeKey = "pushType"
        val pushTypeMock = "on_create_election"
        val remoteMessageData = mapOf(Pair(pushTypeKey, pushTypeMock))
        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
        every { remoteMessageMock.data } returns remoteMessageData

        val pushType = myMessagingService.getPushType(remoteMessageMock)
        assertEquals(pushTypeMock, pushType)
    }

    @Test
    fun getPushTypeNoData() {
        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
        val exception = assertThrows<Exception> {
            myMessagingService.getPushType(remoteMessageMock)
        }
        assertEquals(exception.message, "pushType is missing from remoteMessage.data: $remoteMessageMock")
    }

    @Test
    fun `getVotePeerActivityIntent$app_debug`() {
        val intent = InjectorUtilsApp.getVotePeerActivityIntent(applicationContextMock, privateKeyMock, electionIdMock)
        assertEquals(intent.extras?.get(Constants.PRIVATE_KEY), privateKeyMock)
        assertEquals(intent.extras?.get("setSupportActionBar"), false)
        assertEquals(intent.extras?.get(Constants.SETUP_ACTION_BAR_WITH_NAV_CONTROLLER), false)
        assertEquals(intent.extras?.get("setContentView"), false)
        assertEquals(intent.extras?.get("election_id"), electionIdMock)
        assertNotNull(intent.flags)
    }

    @Test
    fun `getVotePeerActivityPendingIntent$app_debug`() {
        val intent = InjectorUtilsApp.getVotePeerActivityIntent(applicationContextMock, privateKeyMock, electionIdMock)
        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
        assertNotNull(pendingIntent)
    }

    @Test
    fun `createNotificationBuilder$app_debug`() {
        val content = "election_id"
        val intent = InjectorUtilsApp.getVotePeerActivityIntent(applicationContextMock, privateKeyMock, electionIdMock)
        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
        val notificationBuilder = myMessagingService.createNotificationBuilder(content, pendingIntent)
        assertNotNull(notificationBuilder)
    }

    @Test
    fun `createNotification$app_debug`() {
        val content = "election_id"
        val intent = InjectorUtilsApp.getVotePeerActivityIntent(applicationContextMock, privateKeyMock, electionIdMock)
        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
        val notificationBuilder = myMessagingService.createNotificationBuilder(content, pendingIntent)
        myMessagingService.createNotification(notificationBuilder)
    }
}
