package info.bitcoinunlimited.voting

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Hash
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.PayAddressType
import bitcoinunlimited.libbitcoincash.PayDestination

object VotePeerMock {
    fun mockPrivateKey(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }

    fun mockP2pkhAddress(chain: ChainSelector, privateKey: ByteArray): PayAddress {
        val pkh = toPKH(PayDestination.GetPubKey(privateKey))
        return PayAddress(chain, PayAddressType.P2PKH, pkh)
    }

    private fun toPKH(publicKey: ByteArray): ByteArray {
        return Hash.hash160(publicKey)
    }
}
