package info.bitcoinunlimited.voting

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class SplashScreenActivityTest {

    private lateinit var scenario: ActivityScenario<SplashScreenActivity>

    @Test
    fun verifyUi() {
        scenario = launchActivity()
        onView(withId(R.id.splash_screen_title))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.splash_screen_title))
            .check(matches(withText("VotePeer: On-chain elections.")))
        onView(withId(R.id.splash_screen_progress_bar))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.bitcoin_unlimited))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }
}
